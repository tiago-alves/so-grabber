#!/bin/bash

	# **********************************************************************
	#
	# 	tigre project grabber - bash version
	# 	so-grabber <userid> <push/pull>
	#
	# 	Place this script inside a folder with the same name
	# 	as the main project.
	#

        project_dir="so-final"
        project_subdir="parte-1"

	# 	For example:
	#
	# 		|-- Final-Project		<- change in "project_dir"
	# 			|------ THIS-SCRIPT-HERE.sh
	# 			|------ part-1		<- change in "project_subdir"
	# 			|------ part-2
	# 			|------ part-3
	#
	# **********************************************************************


currentDir=$( pwd | grep -o '[^/]*$' )

if [ $currentDir != $project_dir ]; then
    echo "ERROR: Place this script in a folder with the same name as the project"
    exit
fi

if [ $# -lt 2 ]; then
    echo "Usage: $0 <userID> <pull/push>"
fi

remote_dir="~/"
username=$1

if [ "$2" == "pull" ]; then
    scp -r ${username}@tigre.iul.lab:${remote_dir}${project_dir}/$project_subdir .
fi

if [[ "$2" == "push" ]]; then

    echo "Are you sure about pushing the files to the remote server?"
    echo "Overwritten files cannot be recovered"
    echo "type <Y/n>"
    read confirmation

    if [ "$confirmation" == "Y" ]; then
        scp -r ./$project_subdir ${username}@tigre.iul.lab:${remote_dir}${project_dir}
    fi
fi
