# Tigre Project Grabber
__________________________

    (Windows)   so-grabber.bat
    (Linux)     so-grabber.sh

### How to use:

- Place the script in your main project folder
- Change the variable "project_dir" to the same name
- Change the variable "project_subdir" to the current project that you are working on
- Run it

``so-grabber <userID> <push/pull>``
