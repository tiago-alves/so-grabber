@echo off

	rem :**********************************************************************
	rem :
	rem :	tigre project grabber - batch version
	rem :	so-grabber <userid> <push/pull>
	rem :
	rem :	Place this script inside a folder with the same name
	rem :	as the main project.
	rem :
			set project_dir=so-final
			set project_subdir=parte-1

	rem :	For example:
	rem :
	rem :		|-- Final-Project		<- change in "project_dir"
	rem :			|------ THIS-SCRIPT-HERE.BAT
	rem :			|------ part-1		<- change in "project_subdir"
	rem :			|------ part-2
	rem :			|------ part-3
	rem :
	rem :**********************************************************************


	rem Script Header

	rem grabs tree dir (current folder)
	for %%I in (.) do set current_folder=%%~nxI
	set remote_dir=~/
	set debug=

:main	

	if not "%project_dir%"=="%current_folder%" (
		
		echo Error: Place this script in a folder with the same name as the main project
		if defined debug echo Current folder is "%current_folder%"
		
		>NUL pause
		exit /b
	)
	
	if "%1"=="" goto syntax_help
	if "%2"=="" goto syntax_help
	
    if "%2"=="push" (

	:redo
		echo Are you sure about pushing the files to the remote server?
		echo Overwritten files cannot be recovered

		rem choice /n /cs /c Yn

		setlocal EnableDelayedExpansion
		set /p choice=Type "Yes" to proceed, type "no" to cancel:

		if defined debug echo Choice var: "!choice!"

		if "!choice!" == "Yes" (
			echo scp -r %cd%\%project_subdir% %1@tigre.iul.lab:%remote_dir%%project_dir%/
			scp -r %cd%\%project_subdir% %1@tigre.iul.lab:%remote_dir%%project_dir%/
			exit /b
		)

		if /i "!choice!" == "no" (
			echo Operation was cancelled
			exit /b
		)

		goto redo

    )
	
    if "%2"=="pull" (
		if defined debug echo %1@tigre.iul.lab:%remote_dir%%project_dir%/%project_subdir% %cd%
		start /b /wait scp -r %1@tigre.iul.lab:%remote_dir%%project_dir%/%project_subdir% %cd%
    )

	exit /b

:syntax_help

	rem echo|set /p ="STRING" avoids breakline
	echo|set /p ="Usage: %~n0 <userid> <push/pull>"
	exit /b
